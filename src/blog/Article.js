import React from 'react';

import MarkdownComponent from '../util/MarkdownComponent'
import './Article.css';

const Article = ({ source }) => (
  <MarkdownComponent className="Article" source={source} />
)

export default Article;
