import React from 'react';

import MarkdownComponent from '../util/MarkdownComponent'
import Article from './Article'
import blog from './blog.md'

// Import all md files in "articles" folder
let blogArticles = [];

function importAll(r, arr) {
  r.keys().forEach(key => arr.push(r(key)));
}

importAll(require.context('./articles/', true, /\.md$/), blogArticles);

const Blog = () => (
  <section>
    <MarkdownComponent source={blog} />
    {blogArticles.map((article, key) => (
      <Article source={article} key={key} />
    ))}
  </section>
)

export default Blog;
