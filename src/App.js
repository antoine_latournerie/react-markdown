import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';
import logo from './logo.svg';
import Blog from './blog/Blog'
import Home from './home/Home'

const App = () => (
  <Router>
    <div className="App">
      <Header />    
      <Route exact path="/" component={Home} />
      <Route path="/blog" component={Blog} />
    </div>
  </Router>
)

const Header = () => (
  <header className="App-header">
    <img src={logo} className="App-logo" alt="logo" />
    <h1 className="App-title">Welcome to React</h1>
  </header>
)

export default App;
