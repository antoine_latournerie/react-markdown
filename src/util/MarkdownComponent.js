import 'highlight.js/styles/darkula.css';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { Link } from 'react-router-dom';
import Highlight from 'react-highlight';

const RouterLink = ({ href, children }) => (
  href.match(/^(https?:)?\/\//)
    ? <a href={href} target="_blank">{children}</a>
    : <Link to={href}>{children}</Link>
)

const HighlightedCode = ({ value, language }) => (
  <Highlight className={language}>
    {value}
  </Highlight>
)

const MarkdownComponent = (props) => (
  <ReactMarkdown {...props} renderers={{ link: RouterLink, code: HighlightedCode }} />
)

export default MarkdownComponent;
