import React from 'react';

import MarkdownComponent from '../util/MarkdownComponent'
import home from './home.md'

const Home = () => <MarkdownComponent source={home} />

export default Home;
